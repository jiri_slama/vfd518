// Wemos firmware v1.3 for VFD 4-digit clock with HV518 driver
// (c) jsl 2019, 2020

#include <WiFiManager.h>
#include <ezTime.h>

#define LATCH  14               // HV518.latch
#define CLOCK  12               // HV518.clock
#define DATA   13               // HV518.data
#define BLANK  16               // HV518.blank
#define BUTTON  2               // button (gnd, no pullup)

#define ON_HR   6               // display on at ON_HR
#define OFF_HR  1               // display off at OFF_HR
#define UPDATE_PERIOD  21468    // NTP resync interval
#define ESP_RESTART    11000    // restart at 03:03:20

int ldr;                        // ldr sensor value
int pwr = 0;                    // current display intensity: 0 = max, 1024 = off
byte hh, mm, ss, dd, mo, but;

// 7-segment shapes
unsigned long segments[] = {
    B00111111, B00000110, B01011011, B01001111, // 0 1 2 3
    B01100110, B01101101, B01111101, B00000111, // 4 5 6 7
    B01111111, B01101111, B01110111, B01111100, // 8 9 A b
    B00111001, B01011110, B01111001, B01110001, // C d E F
    B00000000, B01000000, B00111000, B01010100, //   - L n
    B01011100                                   // o
};

#define DASH 17
#define __L  18
#define __n  19
#define __o  20

// bits to serial HV output mapping
byte bit_order[32] = {
    15,  7,  7,  7, 24, 29, 25, 30, 28, 26, 27, 16, 21, 17, 22, 20,
    18, 19,  8, 13,  9, 14, 12, 10, 11,  0,  5,  1,  6,  4,  2,  3
};

// display four numbers (left to right) and symbols for SEC and DATE
void disp_nums (byte x, byte y, byte z, byte w, byte sec, byte dot) {
    disp_bits((segments[x] << 24) | (segments[y] << 16) | (segments[z] << 8) | (segments[w]) | (sec ? 0x8000 : 0) | (dot ? 0x80 : 0));
}

// shift data out to the hv518 driver
void disp_bits (unsigned long bits) {
    digitalWrite(LATCH, 0);
    for (int i = 0; i < 32; i++) {
        digitalWrite(DATA, (bits & (1L << bit_order[i])) ? HIGH : LOW);
        digitalWrite(CLOCK, 1);
        digitalWrite(CLOCK, 0);
    }
    digitalWrite(LATCH, 1);
}

// change displayed values with dim-and-brighten effect
void chng_nums (byte x, byte y, byte z, byte w, byte sec, byte dot) {
    #define STEPS 10
    int i, c = (1024 - pwr) / STEPS;
    // dim the display
    for (i=1; i < STEPS; i++) {
        analogWrite(BLANK, pwr + c * i);
        delay(400 / STEPS);
    }
    analogWrite(BLANK, 1024); // display off
    // set the new value
    disp_nums(x, y, z, w, sec, dot);
    delay(100);
    // and show it
    for (i=1; i < STEPS; i++) {
        analogWrite(BLANK, 1024 - c * i);
        delay(400 / STEPS);
    }
    analogWrite(BLANK, pwr);  // original intensity value
}

// in WiFi config mode show "ConF"
void configModeCallback (WiFiManager *myWiFiManager) {
    disp_nums(0x0C, __o, __n, 0x0F, 0, 0);
}

Timezone myTz;
WiFiManager wifiManager;

void setup() {
    pinMode(LATCH, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    pinMode(DATA,  OUTPUT);
    pinMode(BLANK, OUTPUT);
    digitalWrite(BLANK, LOW);
    analogWrite(BLANK, 512);
    pinMode(BUTTON, INPUT_PULLUP);

    Serial.begin(115200);
    wifiManager.setAPCallback(configModeCallback);

    // start WiFi config portal if button pressed after power on
    for (int i=0; i<10; i++) {
        if (!digitalRead(BUTTON)) wifiManager.startConfigPortal("VFD CLOCK");
        delay(300);
        disp_nums(i, i, i, i, i%2, i%3);
    }

    disp_nums(DASH, DASH, DASH, DASH, 0, 0);
    wifiManager.autoConnect("VFD CLOCK");
    disp_nums(DASH, DASH, DASH, DASH, 1, 1);

    WiFi.printDiag(Serial);

    setServer("0.cz.pool.ntp.org");
    setInterval(UPDATE_PERIOD);
    setDebug(INFO);
    waitForSync();
    myTz.setLocation("Europe/Prague");
    myTz.setDefault();
}

void loop() {
    events(); // eztime library events handling

    if (!digitalRead(BUTTON)) but = 1;       // check for button

    if (timeStatus() != timeNotSet) {
        if (secondChanged()) {
            // brightness control
            ldr = analogRead(A0);
            pwr = (ldr / 10) * 10;
            analogWrite(BLANK, pwr);

            hh = hour();
            mm = minute();
            ss = second();
            dd = day();
            mo = month();

            if (hh * 3600 + mm * 60 + ss == ESP_RESTART) ESP.restart();

            if (hh >= OFF_HR && hh < ON_HR)  disp_nums(BLANK, BLANK, BLANK, BLANK, ss & 1, 0);               // no display
            else if (but)                    disp_nums(ldr/1000, (ldr/100)%10, (ldr/10)%10, ldr%10, 1, 0);   // debug: on button show ldr value
            else if (ss == 50)               chng_nums(dd / 10, dd % 10, mo / 10, mo % 10, ss & 1, 1);       // 50th sec - change to date
            else if (ss > 50 && ss < 55)     disp_nums(dd / 10, dd % 10, mo / 10, mo % 10, ss & 1, 1);       // 51-54th sec - show date, blink with SEC symbol
            else if (ss == 55)               chng_nums(hh / 10, hh % 10, mm / 10, mm % 10, ss & 1, ss & 1);  // 55th sec - change to time
            else                             disp_nums(hh / 10, hh % 10, mm / 10, mm % 10, ss & 1, ss & 1);  // otherwise - show time, blink with both symbols

            but = 0;
        }
    }
    else disp_nums(0x0F, 0x0A, 1, __L, 0, 0); // no time info, disp "FAIL"
}
